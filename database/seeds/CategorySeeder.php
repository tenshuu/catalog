<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 20) as $index) {
            DB::table('categories')->insert([
                'category' => $faker->word,
                'parent_id' => $faker->numberBetween($min = 1, $max = 15),
                'level' => $faker->numberBetween($min = 0, $max = 1),
            ]);
        }

    }
}
