<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('products')->insert([
                'name' => $faker->word,
                'description' => $faker->sentence(3),
                'price' => $faker->numberBetween($min = 0, $max = 99999),
                'amount' => $faker->numberBetween($min = 0, $max = 9999),
                'category_id' => $faker->numberBetween($min = 1, $max = 19),
            ]);
        }
    }
}
