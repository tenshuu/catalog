<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 * Class Product
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $description
 * @property double $price
 * @property int $amount
 * @property int $category_id
 *
 * @mixin \Eloquent
 */
class Product extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'price',
        'amount',
        'category_id',
    ];

    public function rules()
    {
        return [
            'name' => 'required|min:1|max:255',
            'description' => 'max:1000',
        ];
    }

    public function createProduct(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $product = new Product();
            $product->fillProduct($request, $product);
            $product->save();
            return response()->json('OK', 200);
        }
    }

    public function updateProduct(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $product->fillProduct($request, $product);
            $product->save();
            return response()->json('OK', 200);
        }
    }

    public function fillProduct(Request $request, Product $product)
    {
        $product->fill(array(
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'amount' => $request->input('amount'),
            'category_id' => $request->input('category_id')
        ));
    }
}
