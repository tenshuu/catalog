<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class Category
 *
 * @package App\Models
 * @property int $id
 * @property string $category
 * @property int $parent_id
 * @property int $level
 *
 * @mixin \Eloquent
 */
class Category extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'category',
        'parent_id',
        'level',
    ];

    public function rules()
    {
        return [
            'category' => 'required|min:1|max:255',
            'parent_id' => 'required',
            'level' => 'required',
        ];
    }

    public function createCategory(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $category = new Category();
            $category->fillCategory($request, $category);
            $category->save();
            return response()->json('OK', 200);
        }
    }

    public function updateCategory(Request $request, Category $category)
    {
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $category->fillCategory($request, $category);
            $category->save();
            return response()->json('OK', 200);
        }
    }

    public function fillCategory(Request $request, Category $category)
    {
        $category->fill(array(
            'category' => $request->input('category'),
            'parent_id' => $request->input('parent_id'),
            'level' => $request->input('level')
        ));
    }
}
