<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends ApiController
{
    public function index()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $product = Product::query()
            ->orderBy('name')
            ->get();

        return $this->sendResponse($product, 'OK', 200);
    }

    public function getOne(Product $product)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $product = Product::query()
            ->where('id', '=', $product->id)
            ->get();

        return $this->sendResponse($product, 'OK', 200);
    }

    public function store(Request $request)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $product = new Product();
        return $product->createProduct($request);
    }

    public function delete(Product $product)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $product = Product::query()
            ->where('id', '=', $product->id)
            ->delete();

        return $this->sendResponse($product, 'OK', 200);
    }

    public function update(Request $request, Product $product)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $product->updateProduct($request, $product);
        return $this->sendResponse($product, 'OK', 200);
    }
}
