<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends ApiController
{
    public function index()
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $category = Category::query()
            ->where('level', '=', '0')
            ->orderBy('category')
            ->get();

        return $this->sendResponse($category, 'OK', 200);
    }

    public function store(Request $request)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $category = new Category();
        return $category->createCategory($request);
    }

    public function delete(Category $category)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $category = Category::query()
            ->where('id', '=', $category->id)
            ->delete();

        return $this->sendResponse($category, 'OK', 200);
    }

    public function update(Request $request, Category $category)
    {
        try {
            $user = auth()->userOrFail();
        } catch (\Tymon\JWTAuth\Exceptions\UserNotDefinedException $e) {
            return response()->json(['error' => $e->getMessage()]);
        }

        $category->updateCategory($request, $category);
        return $this->sendResponse($category, 'OK', 200);
    }

}
