Каталог товаров
---------------

О  приложении
-------------

Индивидуальное задание "Католог товаров" бэкенд-разработчика Академии "Умный мир".

Установка
---------

Чтобы скачать проект с gitlab используйте следующую команду:

git clone http://gitlab.com/tenshuu/catalog

Выполните команды:

composer install

php artisan key:generate

Создайте базу данных для проекта в СУБД Postgresql.

Подключите базу данных к проекту в файле .env, находящемся в корневой папке проекта:

DB_CONNECTION=pgsql

DB_HOST=хост

DB_PORT=5432

DB_DATABASE=название_базы_данных

DB_USERNAME=имя_пользователя_базы_данных

DB_PASSWORD=пароль_пользователя_базы_данных

Создайте таблицы базы данных и ее поля с помощью команды:

php artisan migrate

Настройка веб-сервера nginx+unit
--------------------------------

Измените следующие поля в файле корневая_папка_проекта/services/nginx.conf:

server_name имя_домена;

proxy_pass http://имя_домена:8080/;

root /путь_до_папки_public_проекта;


Переместите файл nginx.conf в папку /etc/nginx/conf.d.

Измените следующие поля в файле корневая_папка_проекта/services/config.json:

"root": "/путь_до_папки_public_проекта",

"file": "/etc/php/текущая_версия_php_7.4+/cli/php.ini
"

Используйте команду:

curl -X PUT -d @/путь_к_файлу_config.json --unix-socket /путь_до_control.unit.sock http://localhost/config

Добавьте домен в список доступных доменов в файле /etc/hosts (стандартно).

Перезапустите nginx и unit.

Дополнительно
-------------

Документация по laravel:

https://laravel.com/docs/7.x

Документация по Postgresql:

https://postgrespro.ru/docs/postgresql

Документация по nginx:

https://nginx.org/ru/docs/

Документация по nginx-unit:

https://unit.nginx.org/
