<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('product')->group(function () {
    Route::get('/show', 'ProductController@index'); // Show all products
    Route::get('/{product}', 'ProductController@getOne')->where('product', '[0-9]+'); // Show one product
    Route::post('/create', 'ProductController@store'); // Add new product
    Route::delete('/delete/{product}', 'ProductController@delete')->where('product', '[0-9]+'); // Delete the product
    Route::post('/update/{product}', 'ProductController@update')->where('product', '[0-9]+'); // Update the product
});

Route::prefix('category')->group(function () {
    Route::get('/show', 'CategoryController@index'); // Show all categories
    Route::post('/create', 'CategoryController@store'); // Add new category
    Route::delete('/delete/{category}', 'CategoryController@delete')->where('category', '[0-9]+'); // Delete the category
    Route::post('/update/{category}', 'CategoryController@update')->where('category', '[0-9]+'); // Update the category
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@registration');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
